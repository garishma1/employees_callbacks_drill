let {retrievingDataForIds,groupDataBasedOnCompanies,allDataForCompanyPowerPuffBrigade,removeEntryWithId2,sortingDataBasedOnCompany,swappingPositionsOfCompanies,addingBirthDayDateInformation}
=require("./problems.cjs");

let fs=require("fs");

function main(){
    try{
    fs.readFile("./data.json","utf-8",(error,employeedata)=>{
        if(error){
            console.log(error);
        }else{
        
            console.log("Data read successfully");

            let employessData=JSON.parse(employeedata);
            let employees=employessData.employees;

            // Retrieving Data for ids

            retrievingDataForIds(employees,(error,retrievingData)=>{
                if(error){
                    console.log(error);
                }else{
                    fs.writeFile("./output/retrieving-data-for-ids.json",JSON.stringify(retrievingData),(error)=>{
                        if(error){
                            console.log(error);
                        }else{
                        
                            console.log("Data retrived successfully");

                            // Group data by companies

                            groupDataBasedOnCompanies(employees,(error,data)=>{
                                if(error){
                                    console.log(error);
                                }else{
                                    fs.writeFile("./output/group-data-based-On-companies.json",JSON.stringify(data),(error)=>{
                                        if(error){
                                            console.log(error);
                                        }else{
                                    
                                            console.log("Data grouped by companies");
                                            // all Data for company Power Puff brigade

                                            allDataForCompanyPowerPuffBrigade(employees,(error,data)=>{
                                                if(error){
                                                    console.log(error);
                                                }else{
                                                    fs.writeFile("./output/all-data-for-company-power-puff-Brigade.json",JSON.stringify(data),(error)=>{
                                                        if(error){
                                                            console.log(error)
                                                        }else{
                                                        
                                                            console.log("Power Puff data");

                                                            // Remove Entry with id2 

                                                            removeEntryWithId2(employees,(error,data)=>{
                                                                if(error){
                                                                    console.log(error);
                                                                }else{
                                                                    fs.writeFile("./output/remove-Entry-with-id2.json",JSON.stringify(data),(error)=>{
                                                                        if(error){
                                                                            console.log(error);
                                                                        }else{
                                                                    
                                                                            console.log("id removed successfully");

                                                                            //sorting data based on company

                                                                            sortingDataBasedOnCompany(employees,(error,data)=>{
                                                                                if(error){
                                                                                    console.log(error);
                                                                                }else{
                                                                                    fs.writeFile("./output/sorting-data-based-on-company.json",JSON.stringify(data),(error)=>{
                                                                                        if(error){
                                                                                            console.log(error);
                                                                                        }else{
                                                                                            console.log("sorted data");

                                                                                            // Swaping id with 92 and 93

                                                                                            swappingPositionsOfCompanies(employees,93,92,(error,data)=>{
                                                                                                if(error){
                                                                                                    console.log(error);
                                                                                                }else{
                                                                                                    fs.writeFile("./output/swapping-positions-of-companies.json",JSON.stringify(data),(error)=>{
                                                                                                        if(error){
                                                                                                            console.log(error);
                                                                                                        }else{
                                                                                                            
                                                                                                            console.log("data swapped");

                                                                                                            // Adding birth day date to even ids

                                                                                                            addingBirthDayDateInformation(employees,(error,data)=>{
                                                                                                                if(error){
                                                                                                                    console.log(error)
                                                                                                                }else{
                                                                                                                    fs.writeFile("./output/adding-BirthDay-date-information.json",JSON.stringify(data),(error)=>{
                                                                                                                        if(error){
                                                                                                                            console.log(error);
                                                                                                                        }else{
                                                                                                                            console.log("DOB added");
                                                                                                                        }
                                                                                                                    })
                                                                                                                }
                                                                                                            })
                                                                                                        }
                                                                                                    })
                                                                                                }
                                                                                            })
                                                                                        }
                                                                                    })
                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })


                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    })
}catch(error){
    console.log(error);
}
}

main()


                                                                                    


