function retrievingDataForIds(arr,cb){
    try{
    let ids=[2,13,23];
    let retrievingData=arr.reduce((acc,cv)=>{
        ids.forEach((id)=>{
            if(id===cv.id){
                acc.push(cv);
            };
        });
        return acc;

    },[]);

    cb(null,retrievingData);
}catch(error){
    console.log(error);
}

};

function groupDataBasedOnCompanies(arr,cb){
    try{
        let groupData=arr.reduce((acc,cv)=>{
        if(acc[cv.company]){
            acc[cv.company].push(cv);
        }else{
            acc[cv.company]=[];
            acc[cv.company].push(cv);
        }
        return acc;

    },{});
    cb(null,groupData);
}catch(error){
    console.log(error);
}

};

function allDataForCompanyPowerPuffBrigade(arr,cb){
    try{
    let powerPuffBrigadeData=arr.reduce((acc,cv)=>{
        if(cv.company==="Powerpuff Brigade"){
            acc.push(cv);
        }
        return acc;
    },[]);

    cb(null,powerPuffBrigadeData);
}catch(error){
    console.log(error);
}
}

function removeEntryWithId2(arr,cb){
    try{
    let removingId2=arr.reduce((acc,cv)=>{
        if(cv.id!==2){
            acc.push(cv)
            
        };
        return acc;
    },[]);

    cb(null,removingId2);
}catch(error){
    console.log(error);
}
}

function sortingDataBasedOnCompany(arr,cb){
    try{
    let sortData=arr.sort((a,b)=>{
        if(a.company===b.company){
            return a.id-b.id;
        }else{
            return a.company - b.company;
        }
    });

    cb(null,sortData);
}catch(error){
    console.log(error);
}
} 

function swappingPositionsOfCompanies(arr,id1,id2,cb){
    try{
    let index1=arr.findIndex((value)=> value.id===id1);
    let index2=arr.findIndex((value)=> value.id===id2);

    if(index1 !== -1 && index2 !== -1){
        [arr[index1],arr[index2]]=[arr[index2],arr[index1]];
    }else{
        console.log("ids not present");
    }

    cb(null,arr);
}catch(error){
    console.log(error);
}

    
}

function addingBirthDayDateInformation(arr,cb){
    try{
    let addingBirthDayDate=arr.reduce((acc,cv)=>{
        if(cv.id%2===0){
            cv["DOB"]=new Date();
            acc.push(cv);
        }else{
            acc.push(cv);
        }
        return acc

    },[]);

    cb(null,addingBirthDayDate);
}catch(error){
    console.log(error);
}
};

module.exports ={retrievingDataForIds,groupDataBasedOnCompanies,allDataForCompanyPowerPuffBrigade,removeEntryWithId2,sortingDataBasedOnCompany,swappingPositionsOfCompanies,addingBirthDayDateInformation}
